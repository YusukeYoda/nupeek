using System.Data.Services;
using System.ServiceModel.Activation;
using System.Web.Routing;
using Ninject;
using NuGet.Server;
using NuGet.Server.DataServices;
using NuGet.Server.Infrastructure;

#if DEBUG || DEPLOY

using NuGet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Services.Common;
using System.Data.Services.Providers;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Hosting;
using NuGetRoutes = NuPeek.DataServices.NuGetRoutes;

[assembly: WebActivator.PreApplicationStartMethod(typeof(NuGetRoutes), "Start")]

namespace NuPeek.DataServices {
    public static class NuGetRoutes {
        public static void Start() {
            MapRoutes(RouteTable.Routes);
            NinjectBootstrapper.Kernel.Rebind<IServerPackageRepository>().ToConstant(new StatsServerPackageRepository(PackageUtility.PackagePhysicalPath));
        }

        private static void MapRoutes(RouteCollection routes) {
            // The default route is http://{root}/nuget/Packages
            var factory = new DataServiceHostFactory();
            var serviceRoute = new ServiceRoute("nuget", factory, typeof(Packages));
            serviceRoute.Defaults = new RouteValueDictionary { { "serviceType", "odata" } };
            serviceRoute.Constraints = new RouteValueDictionary { { "serviceType", "odata" } };
            routes.Add("nuget", serviceRoute);
        }

        private static PackageService CreatePackageService() {
            return NinjectBootstrapper.Kernel.Get<PackageService>();
        }
    }

    public class StatsUtil
    {
        public static readonly string StatsRoot = HostingEnvironment.MapPath("~/Stats");
    }

    public class StatsPackageContext
    {
        private readonly IServerPackageRepository _repository;

        //nuget/Packageで返す情報にDownloadCountとVersionDownloadCountを追加
        public IQueryable<Package> Packages
        {
            get
            {
                var packages = _repository.GetPackages().ToArray().Select(package =>
                {
                    var metadataPackage = _repository.GetMetadataPackage(package);
                    var path = Path.Combine(StatsUtil.StatsRoot, $"{package.Id}.{package.Version.ToString()}");

                    if (!File.Exists(path))
                    {
                        return metadataPackage;
                    }

                    int versionDownloadCount;

                    if (int.TryParse(File.ReadAllText(path), out versionDownloadCount))
                    {
                        metadataPackage.VersionDownloadCount = versionDownloadCount;
                    }

                    return metadataPackage;
                }).ToArray();

                var downloadCounts = packages.GroupBy(package => package.Id).Select(package =>
                {
                    return new
                    {
                        package.Key,
                        DownloadCount = package.Sum(p => p.VersionDownloadCount)
                    };
                }).ToDictionary(x => x.Key, y => y.DownloadCount);

                return packages.Select(package =>
                {
                    package.DownloadCount = downloadCounts[package.Id];
                    return package;
                }).AsQueryable();
            }
        }

        public StatsPackageContext(IServerPackageRepository repository)
        {
            _repository = repository;
        }
    }

    //StatsPackageContextを利用するためにPackageクラスを本家より拝借
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class Packages : DataService<StatsPackageContext>, IDataServiceStreamProvider, IServiceProvider
    {
        private IServerPackageRepository Repository { get; } = NinjectBootstrapper.Kernel.Get<IServerPackageRepository>();
        public int StreamBufferSize { get; } = 64000;

        public static void InitializeService(DataServiceConfiguration config)
        {
            config.SetEntitySetAccessRule("Packages", EntitySetRights.AllRead);
            config.SetEntitySetPageSize("Packages", 100);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V2;
            config.UseVerboseErrors = true;
            RegisterServices(config);
        }

        internal static void RegisterServices(IDataServiceConfiguration config)
        {
            config.SetServiceOperationAccessRule("Search", ServiceOperationRights.AllRead);
            config.SetServiceOperationAccessRule("FindPackagesById", ServiceOperationRights.AllRead);
            config.SetServiceOperationAccessRule("GetUpdates", ServiceOperationRights.AllRead);
        }

        protected override StatsPackageContext CreateDataSource() => new StatsPackageContext(Repository);
        public void DeleteStream(object entity, DataServiceOperationContext operationContext) { throw new NotSupportedException(); }
        public Stream GetReadStream(object entity, string etag, bool? checkETagForEquality, DataServiceOperationContext operationContext) { throw new NotSupportedException(); }

        public Uri GetReadStreamUri(object entity, DataServiceOperationContext operationContext)
        {
            var package = (Package)entity;
            var appSetting = ConfigurationManager.AppSettings["rootUrl"];
            var baseUri = string.IsNullOrWhiteSpace(appSetting)
                ? HttpContext.Current.Request.Url.GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped)
                : appSetting;

            return new Uri(new Uri(baseUri), PackageUtility.GetPackageDownloadUrl(package));
        }

        public string GetStreamContentType(object entity, DataServiceOperationContext operationContext) => "application/zip";
        public string GetStreamETag(object entity, DataServiceOperationContext operationContext) => null;
        public Stream GetWriteStream(object entity, string etag, bool? checkETagForEquality, DataServiceOperationContext operationContext) { throw new NotSupportedException(); }
        public string ResolveType(string entitySetName, DataServiceOperationContext operationContext) { throw new NotSupportedException(); }
        public object GetService(Type serviceType) => serviceType == typeof(IDataServiceStreamProvider) ? this : null;

        [WebGet]
        public IQueryable<Package> Search(string searchTerm, string targetFramework, bool includePrerelease)
        {
            var targetFrameworks = string.IsNullOrEmpty(targetFramework)
                ? new string[0]
                : targetFramework.Split('|');

            return Repository.Search(searchTerm, targetFrameworks, includePrerelease)
                             .Select(package => Repository.GetMetadataPackage(package));
        }

        [WebGet]
        public IQueryable<Package> FindPackagesById(string id) => Repository.FindPackagesById(id).Select(Repository.GetMetadataPackage).AsQueryable();

        [WebGet]
        public IQueryable<Package> GetUpdates(string packageIds, string versions, bool includePrerelease, bool includeAllVersions, string targetFrameworks)
        {
            if (string.IsNullOrEmpty(packageIds) || string.IsNullOrEmpty(versions))
                return Enumerable.Empty<Package>().AsQueryable();

            var trimedPackageIds = packageIds.Trim().Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            var trimedVersions = versions.Trim().Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

            var frameworkNames = !string.IsNullOrEmpty(targetFrameworks)
                ? targetFrameworks.Split('|').Select(VersionUtility.ParseFrameworkName).ToList()
                : null;

            if (trimedPackageIds.Length == 0 || trimedPackageIds.Length != trimedVersions.Length)
                return Enumerable.Empty<Package>().AsQueryable();

            var metadatas = trimedPackageIds.Select((packageId, index) => (IPackageMetadata)new PackageBuilder
            {
                Id = packageId,
                Version = new SemanticVersion(trimedVersions[index])
            });

            return Repository.GetUpdatesCore(metadatas, includePrerelease, includeAllVersions, frameworkNames)
                .AsQueryable()
                .Select(package => Repository.GetMetadataPackage(package));
        }
    }

    public class StatsServerPackageRepository : ServerPackageRepository
    {
        public StatsServerPackageRepository(string path)
            : base(path)
        {
        }

        public StatsServerPackageRepository(IPackagePathResolver pathResolver, IFileSystem fileSystem)
            : base(pathResolver, fileSystem)
        {
        }

        //パッケージダウンロード時にダウンロード件数を保持するために拡張
        public override IEnumerable<IPackage> FindPackagesById(string packageId)
        {
            var packages = base.FindPackagesById(packageId).ToArray();

            foreach (var package in packages)
            {
                var packageDirectory = PathResolver.GetPackageDirectory(package);
                var packagePath = Path.Combine(StatsUtil.StatsRoot, packageDirectory);

                if (!FileSystem.FileExists(packagePath))
                {
                    var bytes = Encoding.UTF8.GetBytes(1.ToString());
                    using (var stream = new MemoryStream(bytes))
                        FileSystem.AddFile(packagePath, stream);
                }
                else
                {
                    int count;
                    var data = File.ReadAllText(packagePath);

                    if (!int.TryParse(data, out count))
                        continue;

                    count++;

                    File.WriteAllText(packagePath, count.ToString());
                }
            }

            return packages;
        }
    }
}
#endif
